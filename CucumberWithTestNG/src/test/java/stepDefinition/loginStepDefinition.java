package stepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class loginStepDefinition {
	WebDriver driver = new ChromeDriver();
	
	@Given("navigate to login page {string}")
	public void navigate_to_login_page(String url) {
		driver.manage().window().maximize();
	    driver.navigate().to(url);
	    System.out.println("navigate to login page "+ url);
	}

	@When("user is navigated to lopin page")
	public void user_is_navigated_to_lopin_page() {
		String expected = "Demo Web Shop. Login";
		if(expected.equals(driver.getTitle()))
	    System.out.println("user is navigated to lopin page");
		else
		System.out.println("user is not navigated to lopin page");
	    
	}

	@Then("user enters details {string} {string}")
	public void user_enters_details(String user, String pswd) {
	    System.out.println("user enters details : "+ user + ", " + pswd);
	    driver.findElement(By.id("Email")).sendKeys(user);
		driver.findElement(By.id("Password")).sendKeys(pswd);
	}

	@And("click login button")
	public void click_login_button() {
		System.out.println("click login button");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@Then("user will navigate to homepage with logged in name")
	public void user_will_navigate_to_homepage_with_logged_in_name() {
	    String expected = "Demo Web Shop";
		if(expected.equals(driver.getTitle()))
	    System.out.println("user is navigated to homepage with logged in name");
		else
			System.out.println("user is not navigated to homepage with logged in name");
	}

	@And("click on logout button")
	public void click_on_logout_button() {
	    System.out.println("click on logout button");
	    driver.findElement(By.linkText("Log out")).click();
	}

	@Then("user will navigate to homepage with logged out")
	public void user_will_navigate_to_homepage_with_logged_out() {
	    String expected = "Demo Web Shop";
		if(expected.equals(driver.getTitle()))
	    System.out.println("user is navigated to homepage with logged out");
		else
			System.out.println("user is not navigated to homepage with logged out");
	}

	@And("close the browser")
	public void close_the_browser() {
	    System.out.println("close the browser");
	    driver.close();
	}
	
}
