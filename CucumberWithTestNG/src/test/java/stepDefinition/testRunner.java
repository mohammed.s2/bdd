package stepDefinition;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
 
@CucumberOptions(tags = "",
features = "src/test/resources/features",                                               ///With TestNG
glue = "stepDefinition",
plugin = { "pretty", "html:target/cucumber-reportss" },
monochrome = true)



public class testRunner extends AbstractTestNGCucumberTests{

}